#ifndef __PIC_10C_DL_LIST_H__
#define __PIC_10C_DL_LIST_H__

#include <stdexcept>  // std::invalid_argument
#include <algorithm>  // std::swap

// Well create our own namespace so that we can use the word 'list'
namespace Pic10C{

  // First we start with the wishlist (a.k.a as plain interface)
  template <typename ItemType>
  class list{
    private:
      // Nested 'NestedNode' class (see nested_node.h)
      #include "nested_node.h"

    public:
      // Nested friend 'iterator' class (see nested_iterator.h)
      #include "nested_iterator.h"

      // The iterator needs to know where this list begins and where
      // it ends. We'll grant friendship to the iterator class instead
      // of implementing accessors that return a raw pointer. 
      friend class iterator;

      // Nested friend 'const iterator' class (see nested_const_iterator.h)
      #include "nested_const_iterator.h"
      friend class const_iterator;

    private:
      // Data fields
      NestedNode* head;
      NestedNode* tail;
      std::size_t num_items;   // <-- optional

    public:
      /** Member functions */
      // The big 4
      list() : head(nullptr), tail(nullptr), num_items(0) { } 


      // *** ITERATORS ARE USED HERE ******************************

      list( const list<ItemType>& source )
        : head(nullptr), tail(nullptr), num_items(0) {
            // We need a const_iterator here. Why???
            for ( const_iterator iter = source.begin() ; iter != source.end() ; ++iter )
                push_back(*iter);
      }

      // **********************************************************


      ~list(){
          while( head != nullptr ){
              NestedNode* current_node = head;
              head = head->next;
              // delete called on local pointer. Why???
              delete current_node; 
          }
      }

      list<ItemType>& operator=( list<ItemType> rhs ){
          swap(*this,rhs);  // <-- Calls specialized swap function
          return *this;
      }


      bool empty() const {
        return ( head == nullptr );
      }

      std::size_t size() {
          return num_items;
      }

      ItemType& front(){
        if ( head == nullptr )
            throw std::invalid_argument("Attempting to call front() on an empty list.");
        return head->data; 
      }

      const ItemType& front() const { 
        if ( head == nullptr )
            throw std::invalid_argument("Attempting to call front() on an empty list.");
        return head->data; 
      }

      ItemType& back(){
        if ( tail == nullptr )
            throw std::invalid_argument("Attempting to call back() on an empty list.");
        return tail->data;
      }

      const ItemType& back() const {
        if ( tail == nullptr )
            throw std::invalid_argument("Attempting to call back() on an empty list.");
        return tail->data;
      }

      void push_front( const ItemType& theData ){
        // Create new node, store data and link it to 
        // nullptr (prev) and head (next).
        head = new NestedNode(theData,nullptr,head);

        // One or more nodes excluding this one?
        if ( head->next ) 
            head->next->prev = head;

        // Are we dealing with an empty list?
        if ( !tail ) 
            tail = head;

        num_items++;
        return;
      }

      void push_back( const ItemType& theData ){
        // At least one node?
        if ( tail ){
            tail->next =  new NestedNode(theData, tail, nullptr);
            tail = tail->next;
            num_items++;
        }
        // Ok, no nodes. Use push_front() instead
        else
            //  numItems is increased here as well
            push_front(theData);  

        return;
      }

      void pop_front() {
        if ( head == nullptr )
	   throw std::invalid_argument("Attempting to call pop_front() on an empty list.");

        // Release memory via temporary object
        NestedNode* toBeDeleted = head;
        head = head->next;
        delete toBeDeleted;

        num_items--;

	// If there is a new head, update its prev pointer.
	// If not, we have a new empty list. Update tail.
	if ( head )
            head->prev = nullptr;
	else
            tail = nullptr;

	return;
      }

      void pop_back(){
        if ( tail == nullptr )
           throw std::invalid_argument("Attempting to call pop_back() on an empty list.");

        // Release memory via temporary object
	NestedNode* toBeDeleted = tail;

	// Rearrange links, release memory, update number 
	// of nodes and check for new status of the list
	tail = tail->prev;
	delete toBeDeleted;
	num_items--;

	if ( tail )
            tail->next = nullptr;
	else
            head = nullptr;

        return;
      }


      // *** ITERATORS ARE USED HERE ******************************
      iterator begin(){
          return iterator(this,head);
      }

      const_iterator begin() const {
          return const_iterator(this,head);
      }

      iterator end(){
          return iterator(this,nullptr);
          // same as
          // return iterator(this,tail->next);
      }

      const_iterator end() const{
          return const_iterator(this,nullptr);
      }

      // insert returns an iterator pointing to the node that
      // was inserted
      iterator insert(iterator position, const ItemType& theData){
          // If inserting at the head or tail, delegate to
          // push_front or push_back. This also updates num_items
          if ( position.current_node == head ){
              push_front(theData);
              return begin();
          }
          else if ( position.current_node == nullptr ) {
              push_back(theData);
              return iterator(this,tail);
          }
          else { // OK, we are somewhere in the middle
              NestedNode* newNode = new NestedNode(
                  theData,
                  position.current_node->prev,
                  position.current_node);

              position.current_node->prev->next = newNode;
              position.current_node->prev = newNode;

              num_items++;
              return iterator(this,newNode);
          }
      }


      // erase returns an iterator pointing at the node
      // that follows (i.e., next) the node that is erased
      iterator erase(iterator position){
          if ( empty() )
              throw std::invalid_argument("Attempting to erase from empty list.");

          if ( position == end() )
              throw std::invalid_argument("Attempting to erase past the last node.");

          iterator toBeReturned = position;
          ++toBeReturned;

          // Delegate approriately if possible
          if ( position.current_node == head )
              pop_front();
          else if ( position.current_node == tail )
              pop_back();
          else {
              NestedNode* toBeDeleted = position.current_node;
              toBeDeleted->prev->next = toBeDeleted->next;
              toBeDeleted->next->prev = toBeDeleted->prev;
              delete toBeDeleted;
              num_items--;
          }

          return toBeReturned;
      }

      // remove erases all nodes in the list whose data field
      // matches the argument of the function
      void remove( const ItemType& theData ){
          iterator iter = begin();
          while ( iter != end() )
              if ( *iter == theData )
                  iter = erase(iter);
              else
                  ++iter;

          return;

          // ***** Defnitely not a good idea!!! Why??? *****
          // for ( iterator iter = begin() ; iter != end() ; ++iter )
          //     if ( *iter  == theData )
          //         erase(iter);

      }

      // **********************************************************


      void swap( list<ItemType>& other ){
          std::swap(head,other.head);
          std::swap(tail,other.tail);
          std::swap(num_items,other.num_items);
          return;
      }

  }; // end of list<ItemType> interface

  // Template specialization of std::swap
  template <typename ItemType>
  inline void swap( list<ItemType>& x, list<ItemType>& y ){
      x.swap(y);
      return;
  }

}// end of Pic10B namespace


#endif
