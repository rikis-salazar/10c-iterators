#ifndef __NESTED_DL_CONST_ITERATOR_H__
#define __NESTED_DL_CONST_ITERATOR_H__

// This is a nested class of Pic10C::list<ItemType>
class const_iterator{
  // Let the parent class have access to all fields and functions
  friend class list<ItemType>;

  private: 
    // A pointer back to the container. This way this 
    // iterator knows its way back to its parent.
    list<ItemType>* parent;

    // And of course a pointer to the Node the iterator points to
    typename list<ItemType>::NestedNode* current_node;


    // The constructor IS PRIVATE. However, the parent class has 
    // access to it. This is standard behavior and it prevents 
    // outsiders from misusing this class. 
    const_iterator(list<ItemType>* theParent, NestedNode* thePosition)
        : parent(theParent), current_node(thePosition) { }


  public:
    // Copy constructor (PUBLIC). Other classes may make a copy. 
    const_iterator( const const_iterator& source ) 
        : parent(source.parent), current_node(source.current_node) { }

    // We are implementing a bidirectional operator. The following 
    // member functions are therefored REQUIRED.

    // Dereference [ operator* (rvalue) ]
    // See also iterator for the lvalue version.
    const ItemType& operator*() const {
        if ( current_node == nullptr )
            throw std::invalid_argument("Attempting to dereference end()");
        return current_node->data;
    }

    // Increment operator [ operator++ (prefix version) ]
    const_iterator& operator++(){
        if ( current_node == nullptr )
            throw std::invalid_argument("Attempting to advance past end()");
        current_node = current_node->next;
        return *this;
    }

    // Increment operator [ operator++ (postfix version) ]
    const_iterator operator++(int unused) {
        const_iterator toBeReturned = *this;
        ++*this;
        return toBeReturned;
    }

    // Decrement operator [ operator-- (prefix version) ]
    const_iterator& operator--(){
        if ( current_node == parent->head ) 
            throw std::invalid_argument("Attempting to move before begin()");
        if ( current_node == nullptr )  
            current_node = parent->tail; 
        else 
            current_node = current_node->prev;
        return *this;
    }

    // Decrement operator [ operator-- (postfix version) ]
    const_iterator operator--(int unused) {
        const_iterator toBeReturned = *this;
        --*this;
        return toBeReturned;
    }



    // The following functions are not requiered but often times
    // they are also provided. 

    // Dereference [ operator-> ] 
    // It should only work if ItemType is a class [or struct].
    const ItemType* operator->() const {
        if ( current_node == nullptr )
            throw std::invalid_argument("Attempting to dereference end()");
        return &(current_node->data);
    }

    // Boolean comparison [ operator== ]
    bool operator==( const const_iterator& rhs ) const { 
        return current_node == rhs.current_node ;
    }

    // Boolean comparison [ operator!= ]
    bool operator!=( const const_iterator& rhs ) const {
        return !operator==(rhs);  // <-- same as !( *this == rhs )
    }

};

#endif
