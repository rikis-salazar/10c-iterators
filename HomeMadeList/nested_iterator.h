#ifndef __NESTED_DL_ITERATOR_H__
#define __NESTED_DL_ITERATOR_H__

// This is a nested class of Pic10C::list<ItemType>
class iterator{
  // Let the parent class have access to all fields and functions
  friend class list<ItemType>;

  private: 
    // A pointer back to the container. This way this 
    // iterator knows its way back to its parent.
    list<ItemType>* parent; 

    // And of course a pointer to the Node the iterator points to
    typename list<ItemType>::NestedNode* current_node;


    // The constructor IS PRIVATE. However, the parent class has 
    // access to it. This is standard behavior and it prevents 
    // outsiders from misusing this class. 
    iterator(list<ItemType>* theParent, NestedNode* thePosition)
        :  parent(theParent), current_node(thePosition) { }


  public:

    // We are implementing a bidirectional operator. The following 
    // member functions are therefored REQUIRED.

    // Dereference [ operator* (lvalue) ]
    // See also const_iterator for the rvalue version.
    ItemType& operator*() const { 
        if ( current_node == nullptr )
            throw std::invalid_argument("Attempting to dereference end()");
        return current_node->data;
    }

    // Increment operator [ operator++ (prefix version) ]
    iterator& operator++(){
        if ( current_node == nullptr )
            throw std::invalid_argument("Attempting to advance past end()");

        current_node = current_node->next;
        return *this;
    }

    // Increment operator [ operator++ (postfix version) ]
    iterator operator++(int unused){
        // Save current position
        iterator toBeReturned = *this;

        // Delegate the task of incrementing the pointer,
        // this also performs range check.
        ++*this;

        // Return old position
        return toBeReturned; 
    }

    // Decrement operator [ operator-- (prefix version) ]
    iterator& operator--(){
        // Are we at 'begin()'?
        if ( current_node == parent->head )
            throw std::invalid_argument("Attempting to move before begin()");
        // Maybe we are at 'end()'?  
        if ( current_node == nullptr )
            current_node = parent->tail;

        // Ok we are somewhere safe... just do the thing and return *this.
        current_node = current_node->prev;
        return *this;
    }

    // Decrement operator [ operator-- (postfix version) ]
    iterator operator--(int unused){
        iterator toBeReturned = *this;
        --*this;
        return toBeReturned;
    }



    // The following functions are not requiered but often times
    // they are also provided. 

    // Dereference [ operator-> ] 
    // It should only work if ItemType is a class [or struct].
    ItemType* operator->() const {
        if ( current_node == nullptr )
            throw std::invalid_argument("Attempting to dereference end()");
        return &(current_node->data);
    }

    // Boolean comparison [ operator== ]
    bool operator==( const iterator& rhs ) const {
        return current_node == rhs.current_node ;
    }

    // Boolean comparison [ operator!= ]
    bool operator!=( const iterator& rhs ) const {
        return !( *this == rhs );
    }

};

#endif
