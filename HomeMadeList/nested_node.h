#ifndef __NESTED_DL_NODE_H__
#define __NESTED_DL_NODE_H__

// This is a nested class of Pic10C::list<ItemType>
class NestedNode{
  
  // Nodes are really managed by the list, hence the friendship.
  friend class list<ItemType>;

  private:
    ItemType data;
    NestedNode* prev;
    NestedNode* next;

  public:
    // The constructor does not request memory, it only stores the
    // date and sets up the links.
    NestedNode( const ItemType& theData,
                NestedNode* p = nullptr,
                NestedNode* n = nullptr ) 
        : data(theData), prev(p), next(n) { }
};

#endif
