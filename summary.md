% Lecture: Iterators (summary)
% Ricardo Salazar
% May 21, 2018

# Definition

## From [cplusplus.com](http://www.cplusplus.com/reference/iterator/):

An `Iterator` ...

> [I]s any object that, pointing to some element in a range of elements (such as
> an array or a container), has the ability to iterate through the elements of
> that range using a set of operators (with at least the increment (`++`) and
> de-reference (`*`) operators).

Note: a _pointer_ is an iterator but an iterator is not always a pointer.


# Coding examples

## The `Pic10A` way

Say you want to double all entries in a vector.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Usage:    v = double_entries(v);   
vector<int> double_entries( std::vector<int> v ){
    vector<int> temp;
    for ( int i = 0 ; i < v.size() ; i++ ){
        temp.push_back( 2 * v[i] );
    }
    return temp;
}

vector<double> double_entries( std::vector<double> v ){
    // Almost the same code as above
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## The `Pic10A` way (cont) 

Issues: 

*   inefficient: a container is created and it is returned by value.

*   `push_back()` is used; this is specific to `vector`-, or `list`-like
    objects.

*   the expression `2 * v[i]` places extra burden on the types this function
    can work with.


## The `Pic10B` way

If you are familiar with template parameters, you might try the following
approach:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Usage:    double_entries(v);   
template <typename Container>
void double_entries( Container& c ){
    Container::iterator it = c.begin();
    while ( it != c.end() ) {
        *it = *it + *it ;
        ++it;
    }
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## The `Pic10B` way (cont)

Improvements:

*   The container is passed by reference, which avoids an extra copy.

*   `push_back()`, and `operator[]` are no longer used.


Issues: 

*   The `Container` type is expected to be able to find its _first_, and its
    _last_ element.

    > An `array`-like container won't benefit from this approach.


## The `Pic10C` way

To make the code more generic use `Iterator` objects instead.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Usage:    double_entries( v.begin(), v.end() );
//           double_entries( std::begin(v), std::end(v) );

template <typename Iterator>
void double_entries( Iterator start, Iterator stop ){
    while ( start != stop ) {
        *start = *start + *start ;
        ++start;
    }
    return;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## The `Pic10C` way (cont)

Improvements:

*   Any standard container can be used:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
list<int> l = {1, 3, 5};
vector<double> v = {3.14, 20.18};
deque<string> d = {"Abby", "Beatriz", "Citlali"};

double_entries( l.begin(), l.end() );
double_entries( v.begin(), v.end() );
double_entries( d.begin(), d.end() );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

## The `Pic10C` way (cont)

*   It even works with an array (use `std::begin`, `std::end`).
     
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
char a[] = {'A', 'B', 'C'};
double_entries( std::begin(a), std::end(a) );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Bottom line:

> Do not pass a container; instead use iterators to pass a _range_.

Want to test this?

> Code available at [http://cpp.sh/7s4nm][cpp-sh].

[cpp-sh]: http://cpp.sh/7s4nm


## Returning containers?

In general this should be avoided. Consider:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Container, typename Iterator>
Container special_items_in( Iterator start, Iterator stop ){
    Container temp;
    for ( ; start != stop ; ++start ){
        ...    // Find special items in range and add to temp
    }
    return temp;
} 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

*   Inefficient: the container is returned by value[^one].

[^one]: In principle, this calls for a copy of the container, but this copy
    might be optimized away.


## Workarounds?!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Container, typename Iterator>
Container& bad_idea( Iterator start, Iterator stop ){
    Container temp;
    for ( ; start != stop ; ++start ){
        ...    // Find ... add to temp
    }
    return temp;
} 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Won't even compile without extra flags (_e.g.,_ `-fpermissive` in `g++` and 
`clang++`).

*   Issue: return by reference of a temporary object.


## Workarounds?! (cont)

Returning an iterator?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Container, typename Iterator>
Container::iterator also_bad( Iterator start, Iterator stop ){
    Container temp;
    for ( ; start != stop ; ++start ){
        ...    // Find ... add to temp
    }
    return temp.begin();
} 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

*   Issue: return of an invalid iterator (it points to an object that might no
    longer exist).


## Workarounds?! (cont)

Returning a pointer?

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Container, typename Iterator>
Container* likely_to_leak( Iterator start, Iterator stop ){
    Container *ptr_to_heap = new Container();
    for ( ; start != stop ; ++start ){
        ...    // Find ... add to temp
    }
    return ptr_to_heap;
} 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Don't do it! The `new` resource is not **bound** to a scope!!!


# Rethink instead of return

## If the container already exists

Instead of locating _all of the special items_, return an iterator pointing to
the first one found (or to the end of the container, if none is found) ...

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Iterator>
Iterator find_special( Iterator start, Iterator stop ) {
    while ( start != stop ){
        if ( is_special(*start) )
            return start;
        ++start;
    }
    return stop; 
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## If the container already exists (cont)

... Couple this approach with a loop to locate the rest of the special items.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
std::vector<int> v = { ... };

vector<int>::iterator it = find_special( v.begin(), v.end() );
while ( it != v.end() ) {
    // process it, then look for next special item
    ...

    it = find_special( ++it, v.end() );
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## If the container does not exist

Request an iterator to _collect_ the special items. This forces the caller to
provide a container as well.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Iterator>
void find_special( Iterator start, Iterator stop,
                   Iterator iter ) {
    while ( start != stop ){
        if ( is_special(*start) ) {
            *iter = *start;
            ++iter;
        }
        ++start;
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

# Iterator categories

## Input iterators

Can be used in sequential input operations. Each value is read only once and
the iterator is incremented; they are useful but also limited as they cannot
store data.  

If `iter` is an input iterator, you can use: 

*   `++iter` and `iter++` to increment it,
*   `*iter` to dereference it (i.e., get the element pointed to), and
*   `operator==`, `operator!=`, to compare it to another iterator.


## Output Iterators

Can also be used in sequential output operations. Each element is written a
value only once and then the iterator is incremented. They are useful but
limited, just like the input ones. They can store data, but cannot read it.  

If `iter` is an output iterator, you can use:

*   `++iter` and `iter++` to increment it, and
*   `*iter = ...` to store data in the location pointed to.


## Output Iterators (subcategories)

1.  _inserters_, where `*iter` is a call to either `push_back`, `push_front`, or
    `insert`, depending on the type of inserter (back inserter, front inserter,
    or regular inserter); and

2.  `ostream` iterators; they let you _point_ to an output stream and insert
    elements into it. _E.g.:_

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
std::ostream_iterator<int> out_iter( std::cout, '\n' );
std::copy( l.begin(), l.end(), out_iter );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

> Results in the contents of the list being displayed to the console, one entry
> per row.


## Forward Iterators

Can be used to access the sequence of elements in a range in the direction that
goes from its beginning towards its end. They have all the functionality of an
input Iterator, and ---if they are not constant iterators--- also the
functionality of output iterators. They also support _saving_ and _reusing_
(multipass).

The following snippet works for forward but fails for single-pass iterators.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
iter_saved = iter;
iter++;
std::cout << "The previous element is " << (*iter_saved) 
          << ", the current element is " << (*iter) << '.';
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


## Bidirectional Iterators 

They can be used to access the sequence of elements in a range in both
directions (towards the end and towards the beginning). They have all the
functionality of Forward Iterators. If `iter` is a bidirectional iterator, you
can use:

*   all forward iterator operations, and
*   `--iter` and `iter--` to decrement it.


## Random Access Iterators

The last category of iterators. They can be used to access elements at an
arbitrary offset position relative to the element they point to, offering the
same functionality as pointers. They have all the functionality of Bidirectional
Iterators. If `iter1` and `iter2` are random access iterators, you can use

*   all bidirectional iterator operations, 
*   standard pointer arithmetic: `iter1 + n`, `iter1 - n`, `iter1 += n`, 
    `iter1 -= n`, and `iter1 - iter2` (**but not** `iter1 + iter1`),
*   offset dereference operator (`operator[](int)` as in `iter1[n]`), and
*   all comparisons: `iter1 > iter2`, `iter1 < iter2`, `iter1 >= iter2`,
    and `iter1 <= iter2`.
