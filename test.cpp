#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
#include <deque>
#include <list>

using std::vector;
using std::list;
using std::deque;
using std::cout;
using std::string;
using std::for_each;

template<typename Container>
void print_it( Container& c ){
    for ( auto& x : c )
        cout << x << " ";
    cout << '\n';
}

template<typename Iterator>
void double_entries( Iterator start, Iterator stop ){
    while ( start != stop ){
        *start = *start + *start;
        ++start;
    }
}

int main(){

    list<int> l = {1, 3, 5};
    vector<double> v = {3.14, 20.18};
    deque<string> d = {"Abby", "Beatriz", "Citlali"};
    char a[] = {'A', 'B', 'C'};

    double_entries( std::begin(l), std::end(l) );
    double_entries( std::begin(v), std::end(v) );
    double_entries( std::begin(d), std::end(d) );
    double_entries( std::begin(a), std::end(a) );


    print_it(l);
    print_it(v);
    print_it(d);
    // print_it(a);    <-- No can do! Code is not generic enough.

    for_each( std::begin(a), std::end(a), [](int c){ cout << c << ' '; } );
    cout << '\n';     // What is this???  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^      

    return 0;
}
