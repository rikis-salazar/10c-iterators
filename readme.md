# Iterators

In this document we take a look at how iterators work and explain
how they can make our code more general. A classification of iterators
and a brief description of each type of iterator is provided here as
well.

### What are `Iterators`, anyway?

From [cplusplus.com](http://www.cplusplus.com/reference/iterator/):

> An iterator is any object that, pointing to some element in a range 
> of elements (such as an array or a container), has the ability to 
> iterate through the elements of that range using a set of operators 
> (with at least the increment (++) and de-reference (*) operators).

Notice that a _raw_ pointer_ **does qualify** as an iterator if your 
definition of object includes a primitive type. Definitions aside, the
important thing here is to notice that, given a container, let it be
_standard_ or _home-made_, we can always add a layer of abstraction to
raw pointers so that they _know how to travel_ through the container.


## Why do Iterators help write more generic code?

To answer this question, let us look at the way your coding
style has likely evolved.

### The `Pic10[AB]` way!

Suppose for a second that you wanted to apply a mathematical operation
to all the elements stored in a container, say a `std::vector<ItemType>`.
Further assume that said operation (e.g., addition) makes sense for
the generic type `ItemType`. To make things precise let us assume you
wanted to "double the value" of all the entries in a "numeric vector".
If you were a _PIC 10A_ student, chances are that you would code something
along the lines of 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10A {
    using namespace std;

    // Usage:    v = double_entries(v);   
    vector<int> double_entries( std::vector<int> v ){
        vector<int> temp;
        for ( int i = 0 ; i < v.size() ; i++ ){
            temp.push_back( 2 * v[i] );
        }
        return temp;
    }

    vector<double> double_entries( std::vector<double> v ){
        // Almost the same code as above
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

While this code seems to work just fine, it is clearly very inefficient:
a container is created inside the function(s) and this container is returned
by value. In addition, the function `push_back()` is called which makes this
code highly inefficient when the size of the vector is very large. Lastly,
while it is certainly true that `2 * v[i]` doubles the value of `v[i]`, if we
wanted to extend the functionality of this function to other numeric types, 
said values would be forced to provide a valid product operation 
(binary `operator*`).

On the other hand, _Pic 10B_ students and their knowledge of Templates are
more likely to improve the code above by writing something along the
lines of

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10B {
    // Usage:    double_entries(v);   
    template <typename Container>
    void double_entries( Container& c ){
        Container::iterator it = c.begin();
        while ( it != c.end() ) {
            *it = *it + *it ;
            ++it;
        }
        return;
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice that in this case the container is passed by reference, which avoids 
an extra copy. Also, gone are the calls to `push_back()`, `operator[]`,
and `operator*`, which make this code more generic: it works for vectors,
lists, and many other containers that provide ways to find the _first_ and
the _last_ element within them. Unfortunately, if we wanted to double all
entries stored in an array, this would not work, as arrays have no way to know
[without extra help] where their last element is located.


### The `Pic10C` way

To make the code above even more generic, we should eliminate the dependence on
the container (e.g., `Container::begin()` and `Container::end()`) and instead
we should only rely on iterators. This can be achieved by passing a
range to the function instead of the container itself.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
namespace Pic10C {
    // Usage:    double_entries( v.begin(), v.end() );   
    template <typename Iterator>
    void double_entries( Iterator start, Iterator stop ){
        while ( start != stop ) {
            *start = *start + *start ;
            ++start;
        }
        return;
    }
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Summarizing: Do not pass a container to a function, pass a range instead.

### How about returning a container?

In general this is also bad and should be avoided. Consider the following 
example

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Container, typename Iterator>
Container special_items_in( Iterator start, Iterator stop ){
    Container temp;
    // Find special items in range and add to temp
    for ( ; start != stop ; ++start ){
        ...
    }
    return temp;
} 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Here, the issue is the return by value. In principle, it calls for a
copy\footnote{Later we will see that \emph{move semantics} eliminate the need
to make this copy.} of the container to be returned.

Just as in the case of passing a container by value, we could try some
_workarounds_:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Container, typename Iterator>
Container& bad_idea( Iterator start, Iterator stop ){
    Container temp;
    // Find ... add to temp
    for ( ; start != stop ; ++start ){
        ...
    }
    return temp;
} 

template <typename Container, typename Iterator>
Container::iterator also_bad( Iterator start, Iterator stop ){
    Container temp;
    // Find ... add to temp
    for ( ; start != stop ; ++start ){
        ...
    }
    return temp.begin();
} 

template <typename Container, typename Iterator>
Container* likely_to_leak( Iterator start, Iterator stop ){
    Container *temp = new Container();
    // Find ... add to temp
    for ( ; start != stop ; ++start ){
        ...
    }
    return temp;
} 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

As the name indicates, `bad_idea` is not good at all. As a matter of fact,
it won't even compile without extra flags (`-fpermissive` in `g++` and 
`clang++`). The issue here is the return by reference of a temporary object.
The reference will be captured outside of `bad_idea`, but an attempt to
access this reference will likely result in a crash.

Something similar occurs with `also_bad`. The iterator being returned will
likely _point_ to an invalid memory address. Lastly, `likely_to_leak` does
avoid the extra copy, but the programmer has to remember to release the
received object at a later time. 

If you are thinking that using a smart pointer here would solve the issue,
look at the way these functions are used in practice:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
// Setup: 
//    l is a std::list<int>
//    v is a std::vector<int>
//    a is an int array with SIZE elements
v = special_items_in< vector<int>, list<int>::iterator >( l.begin(), l.end() );
v = special_items_in< vector<int>, int* >( a , a + SIZE );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

this is because template types cannot be deducted from a returned object,
the compiler can only work its magic with the function parameters. Now,
if you are thinking that you can add an extra parameter just for the 
puprposes of letting the compiler know what is the return type, then
you should pause for a second to realize that this has become really 
complex. There should be a simpler way for sure.

#### If the container already exists...

In this case, it might pay off to reconsider the way we `find_special_items`.
Instead of trying to locate **all** of them, we could return an iterator
pointing to the first special item [if any]; then use an external loop to
locate all of them.

For example, we could try

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Iterator>
Iterator find_special( Iterator start, Iterator stop ) {
    while ( start != stop ){
        if ( is_special(*start) ) {
            return start;
        }
        ++start;
    }
    return stop; 
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

together with

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
std::vector<int> v;
...

vector<int>::iterator iter = find_special( v.begin(), v.end() );
while ( iter != v.end() ) {
    // process iter, then look for next special item
    ...
    iter = find_special( ++iter, v.end() );
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

#### If the container DOES NOT exist...

In this case we want to rethink our approach: we should request an iterator
to _collect_ the special items. Notice that this forces the caller to provide
a container as well. For example we could try

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template <typename Iterator>
void find_special( Iterator start, Iterator stop, Iterator iter ) {
    while ( start != stop ){
        if ( is_special(*start) ) {
            *iter = *start;
            ++iter;
        }
        ++start;
    }
    return; 
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Summarizing: to make our code more generic we should pass an insertion
point (Iterator).

> Notes:
>
> * The function `is_special(...)` needs to be a template function.
>
> * The lines of code inside the `if` block, are equivalent to
> `*iter++ = *start;` in this case `iter` is moved forward, and
> an iterator pointing to the old location is the one that is 
> dereferenced.
>
> * This algorithm assumes that there is enough room for `iter` to 
> advance. Ideally, the container on the receiving end should be 
> at least as big as the container associated to `start` and `stop`.

Regarding this last note, wouldn't it be nice if we could pass an
iterator associated with an empty container, and that we could 
use a `push`/`insert` function to collect the special items? Well,
turns out we actually can pass a special type of iterator that 
can grow its associated container. These types of iterators are 
called **inserters**. They differ from regular iterators in the 
way they behave when the dereference operator [`operator*()`] is called: 
instead of returning a reference to the _pointed to_ object, they
call an  _insert-like_ type of function (e.g., `push_back`,
`push_front`, or `insert`). However, these iterators are only 
available for containers that provide these member functions.
A particular disadvantage of inserter iterators is the fact that
"_they can write, but they cannot read_". Whereas the statement
` *iter = some_value` can be thought of as 
`some_container::insert(some_value)`, the statement `std::cout << *iter`; 
simply makes no sense: it would be more or less equivalent to
`std::cout << void;`, as `void` is the return type of all of the
_insert-like_ functions listed above.


## Iterator categories

Iterators are classified into categories depending on the member 
functions they implement. These categories are:

### Input iterators

These are iterators that can be used in sequential input operations. 
Each value is read only once and the iterator is then incremented.

Input iterators are useful but they are also limited: they cannot
store data. If `iter` is an input iterator, you can use: 

- `++iter` and `iter++` to increment it,
- `*iter` to dereference it (i.e., get the element pointed to), and
- `operator==`, `operator!=`, to compare it to another iterator.

All STL containers can return at least this level of iterator.
Here is an example that displays all elements in a list:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
std::list<int> l;
... // l is populated here

std::list<int>::iterator it;
for( it = l.begin() ; it != l.end() ; ++it )
    std::cout << (*it) << '\n';
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Input iterators cannot be used for write/modify operations. In particular,
the statement `*it = 2017;` is not valid (compare this to _inserters_
described in the previous section).

### Output Iterators

They can be used in sequential output operations.
Each element is written a value only once and then the iterator is 
incremented.

Output iterators are also useful but limited; basically they are the
opposite of input iterators: they can store data, but cannot read it. 
If `iter` is an output iterator, you can use:

- `++iter` and `iter++` to increment it, and
- `*iter = ...` to store data in the location pointed to.

Output iterators are only for storing. If something is no more than an
output iterator, then you cannot read from it with `*iter`, nor can you
compare it against other output iterators with `==` and `!=`.

Given this last statement, it might seem that this type of iterators
is not limited, but actually useless. However, there are two subcategories of 
iterators that are actually quite > useful. These subcategories are

1. _inserters_ (c.f., section above), and
2. `ostream` iterators.

Ostream iterators let you _point_ to an output stream and insert elements
into it. For example, with `l` defined as in the previous snippet, the 
statements

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
std::ostream_iterator<int> out_iter( std::cout, '\n' );
std::copy( l.begin(), l.end(), out_iter );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

result in the contents of the list being displayed to the console, one
entry per row.

#### Wait! Isn't that what the previous example does?

YES! The difference is that in the former case, the contents of `l`
are _read_ via the iterator and then sent to `std::cout`; whereas in the
latter case the iterator is associated to `std::cout` and it is used to
_write_ the contents of `l` to the console. 

Also, note that the generic algorithm `std::copy` uses two sets of iterators:
one pair of input iterators, and one output iterator. A possible implementation
of `std::copy` is listed below.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
template<typename InputIt, typename OutputIt>
OutputIt copy( InputIt start, InputIt stop, OutputIt target_start ) {

    while( start != stop ) 
        *target_start++ = *start++;

    return target_start;
}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

**Side note:**

> `istream` iterators act as a complement to `ostream` iterators. As the name
> suggest, they fall into the category of Input iterators. For example, the
> snippet below can be used to read integers from the console (`std::cin`) and 
> insert them into `l` in the reverse order in which they were read.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
        std::copy( std::istream_iterator<int>( std::cin ), 
                   std::istream_iterator<int>(),
                   front_inserter(l) );
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

> Here, the second argument `std::istream_iterator<int>()`, calls a
> special constructor that creates a pointer to _the end of the input_.


### Forward Iterators

These iterators can be used to access the sequence of elements in a range in
the direction that goes from its beginning towards its end. They have all the
functionality of an input Iterator, and --if they are not constant iterators--
also the functionality of output iterators. They also support _saving_ and 
_reusing_ (multipass). All STL containers support at least forward iterator
types.

The following snippet works for forward iterators but fails for single-pass
iterators (e.g., input or output iterators) as they do not support backing up
and starting over.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
iter_saved = iter;
iter++;
std::cout << "The previous element is " << (*iter_saved) 
          << ", the current element is " << (*iter) << '.';
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

**Note:**

> A iterator that has been stored is only valid if the underlying container
> is not modified. If elements are inserted, or if the container is changed,
> the saved iterator will have undefined behavior.

### Bidirectional Iterators 

They can be used to access the sequence of elements in a range in both
directions (towards the end and towards the beginning). They have all the
functionality of Forward Iterators. If `iter` is a bidirectional iterator,
you can use:

- all forward iterator operations, and
- `--iter` and `iter--` to decrement it.


### Random Access Iterators

The last category of iterators. They can be used to access elements at 
an arbitrary offset position relative to the element they point to,
offering the same functionality as pointers. They have all the functionality
of Bidirectional Iterators. If `iter1` and `iter2` are random access
iterators, you can use

- all bidirectional iterator operations, 
- standard pointer arithmetic: `iter1 + n`, `iter1 - n`, `iter1 += n`, 
  `iter1 -= n`, and `iter1 - iter2` (**but not** `iter1 + iter1`),
- offset dereference operator (`operator[](int)` as in `iter1[n]`), and
- all comparisons: `iter1 > iter2`, `iter1 < iter2`, `iter1 >= iter2`,
  and `iter1 <= iter2`.

