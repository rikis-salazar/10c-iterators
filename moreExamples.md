# Defining your own iterators

**Iterators** are central to the generality and efficiency of
generic algorithms in the **STL** [_Standard Template Library_].
All STL containers (but not the adapters) define

* the **iterator types** for that container (e.g. `iterator`,
  `const_iterator`, `reverse_iterator`, etc.),
* the `begin()`/`end()` member functions for that container.

Containers that do not define the above cannot be used with
generic algorithms. Needless to say that defining iterator
types and member functions for your containers is a very
useful thing to do.

Let us consider a few cases where this is not particularly hard.

## Iterators from member containers

Let us assume that you are managing a Baseball team (e.g., the LA
Dodgers). A simple approach calls for the creation of a class
`MLB_Team` that internally uses a vector of players (`Pelotero`).
[In case you are wondering, _Pelotero_ means Baseball player in
Spanish].

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class MLB_Team{
    private:
        typedef vector< Pelotero > PlayerList;
        PlayerList roster;
    public:
        typedef PlayerList::iterator iterator;
        typedef PlayerList::const_iterator const_iterator;

        iterator begin() { return roster.begin(); }
        iterator end() { return roster.end(); }
        // OTHER MEMBERS HERE
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

Notice that we are not really doing much. Instead we are using a
technique known as *delegation*. I.e., we ask the 
perfectly-well-defined-and-capable real iterators to do the job 
for us; we are simply letting the compiler know that they will
be *our* types of iterators.

As an added benefit, the range-based for loop below should work
just fine.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
MLB_Team LA_Dodgers();
// Add players: Julio Urias, Clayton Kershaw, Corey Seager, etc.

for( auto iter = LA_Dodgers.begin() ; iter != LA_Dodgers.end() ; ++iter )
   // Do something with iter... (e.g., cout << *iter ; )
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

If you are thinking that it should have issues because we did not
define the prefix increment operator, go back to the definition of
the begin and end member functions. Notice that they return 
perfectly-valid-and-capable `std::vector` iterators, which know
how to perform operations like `++iter`, as well as `iter++`.
This is the beauty of delegation.

One final note: the definition of `PlayerList` via `typedef` is not
necessary, but it makes it easy to change containers (e.g., 
`std::list` instead of `std::vector`) with just one line change.

## Iterators from pointers

As we mentioned earlier, _pointers_ are legal iterators. If in our
example above we knew for sure that the number of active players
in a roster cannot exceede a give number, then perhaps a better
way to represent our MLB_Teams would be via a static array. In this
case the class would look like this:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
class MLB_Team{
    private:
        typedef Pelotero PlayerList[25]; // note it is not the other way around? 
        PlayerList roster; // <-- 25 man roster
    public:
        typedef Pelotero* iterator;
        typedef const Pelotero* const_iterator;

        iterator begin() { return &roster[0]; }
        iterator end() { return &roster[25]; }
        // OTHER MEMBERS HERE
};
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

## Iterators from other Iterators

Sometimes a variation on an iterator for a normal container is needed.
For example, you might want to create an iterator that _counts_ things,
or maybe one that _checks_ to make sure that it is within a valid range.

For example, **Musser** and **Saini's** _STL Reference and Tutorial Guide_
gives an example of a counting iterator. On the other hand, although our
previous example [_home made doubly linked list_] performs range checking,
 it does not quite fall into the category of _iterator from iterator_.
However, **Stroustrup's** _The C++ Programming Language, 3rd ed_., gives
an example of a range-checking iterator. 

To effectively define type of iterators, delegation should be relayed upon
a lot. Basically, the constructor for the new iterator should take some
kind of existing iterator, plus whatever other kind of information it needs;
the existing iterator should then be saved in a private data member. Notice
that in these case, `operator++()`, `operator*()` and the like, should be
defined mostly as calls to the same operators on the stored iterator; the 
extra functionality you may need should also be considered when defining
these operators.

Unfortunately, while conceptually simple, this kind of iterator definition
turns out to require a lot of special C++ _incantations_, this is because
it can be hard for the compiler to know when something is a type name. 
If you're trying to do this, definitely get a copy of **Stroustrup** or
some other reference with a detailed example.

### Wait! No example is provided here?

No, but... Your last assignment is precisely about _home-made_ iterators.
